# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
BLOG::Application.config.secret_key_base = 'dbfeaebd953d4e2d0ccfc2c76b6bd8c606b5e5bc708cd55a36775e7c3819bddf28bcb20d9769433b6fff2d283c9e8f64b67dbb539340ae48d87f4a43bb947d16'
